from flask import (
    Blueprint, current_app, request, render_template, redirect, g
)

from ..frameworks.database import connect_mongo, close_mongo, MongoHandler
from werkzeug.security import generate_password_hash, check_password_hash
controller = Blueprint('/auth', __name__, url_prefix='/auth')

#로그인 라우터
@controller.route('/signin', methods=('GET', 'POST'))
def signin():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        mongo_db = g.mongo_db
        mongoHandler = MongoHandler(mongo_db)
        user = mongoHandler.find_one('users',{"username": username})
        if not user :
            print ("없음")
            return "없음"
        elif not check_password_hash(user['password'], password):
            print('비밀번호 틀림')
            return "비밀번호 틀림"
        else:
            if username == 'admin':
                return "admin 페이지로 redirect"
            else :
                return "일반 유저는 admin page가 없을테니..."
    else:
        return render_template("auth/signin.html")

#회원가입 라우터
@controller.route('/signup', methods=('GET', 'POST'))
def signup():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        expired_date = request.form['expired_date']
        mongo_db = g.mongo_db
        mongoHandler = MongoHandler(mongo_db)
        user = mongoHandler.find_one('users', {"username": username})
        if user:
            return "이미 있음!"
        else: 
            userData = {
                "username" : username,
                "password" : generate_password_hash(password),
                "expired_date" : expired_date
            }
            mongoHandler.insert("users", userData)
        return "DB 삽입 완료"
    else:
        return render_template("auth/signup.html")
@controller.before_request
def before_request():
    connect_mongo(current_app)

@controller.teardown_request
def teardown_request(Exception):
    close_mongo()