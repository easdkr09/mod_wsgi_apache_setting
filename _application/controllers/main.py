from flask import (
    Blueprint, current_app, render_template, request
)

controller = Blueprint('/', __name__, url_prefix='/')

@controller.route('')
def main():
    return "main"
#로그인 실패 시 우선 이쪽으로 리다이렉트 되게 한다.
@controller.route('/login_fail')
def login_fail():
    return "login fail"

#로그인 성공 시 리다이렉트는 여기 
@controller.route('login_success')
def login_success():
    return "login_success"