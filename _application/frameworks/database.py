from datetime import datetime
from pymongo import MongoClient
from pymongo.cursor import CursorType
from flask import g

def connect_mongo(app):
    host = app.config['MONGO_HOST']
    user = app.config['MONGO_USER']
    pwd = app.config['MONGO_PASSWORD']
    dbname = app.config['MONGO_DBNAME']
    #mongo client 접속 
    if 'mongo_client' not in g:
        client = MongoClient(host, 
                            username = user,
                            password = pwd)
        g.mongo_client = client
    #db 접속
    if 'mongo_db' not in g:
        g.mongo_db = g.mongo_client[dbname]

    print("connect mongo!!")

def close_mongo():
    mongo_client = g.pop('mongo_client', None)
    if mongo_client is not None:
        mongo_client.close()
    
    print("close mongo!!")
class MongoHandler:
    def __init__(self, _db: MongoClient) :
        self.db = _db

    def insert(self, collection:str, data):
        return self.db[collection].insert_one(data).inserted_id
    
    def insert_many(self, collection:str, data):
        return self.db[collection].insert_many(data).inserted_id

    def find_one(self, collection:str, condition=None):
        return self.db[collection].find_one(condition, {"_id": False})

    def find(self, collection:str, condition=None):
        return self.db[collection].find(condition, {"_id": False}, no_cursor_timeout=True, cursor_type=CursorType.EXHAUST)

    def delete(self, collection:str, condition=None):
        return self.db[collection].delete_one(condition)

    def delete_many(self, collection:str, condition=None):
        return self.db[collection].delete_many(condition)
    
    def update(self, collection:str, filter:str, update:str=None):
        return self.db[collection].update_one(filter=filter, update=update)

    def update_many(self, collection:str, filter:str, update:str=None):
        return self.db[collection].update_many(filter=filter, update=update)
    
    def text_search(self, collection:str, searchText: str):
        return self.db[collection].find({"$text": {"$search":searchText}})



    