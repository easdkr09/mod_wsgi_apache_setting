
def load_configs():
    from . import basic_config, database_config

    return basic_config, database_config